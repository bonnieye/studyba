# Studyba



## BackEnd Versions
python == 3.6.8
flask == 2.0.3
其余可以参考requirements.txt

## 运行flask后端服务 
‘gunicorn -c gunicorn_config.py app:app’

## 访问后端服务
http://43.143.10.225:5000/

## 删除服务
ps -ef | grep gunicorn
kill -9 对应的pid